#-- TEMPLATE [TYPE: artifact]
.next-version_artifact:
  artifacts:
    expire_in: 2 hours
    paths:
      - .next-version

.images_artifact:
  artifacts:
    expire_in: 2 hours
    paths:
      - images

.version_artifact:
  artifacts:
    expire_in: 2 hours
    paths:
      - .version

#-- TEMPLATE [TYPE: base]
.docker_base:
  before_script:
    - apk update
    - apk upgrade
    - apk add --no-cache curl gcc git jq libc-dev libffi-dev make py-pip python-dev openssh-client openssl-dev rsync sudo
    - pip install docker-compose
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  image: docker:git
  services:
    - docker:dind

.gsg_base:
  before_script:
    - apt-get update
    - apt-get install -y curl git jq rsync
    - git fetch --tags
  image: registry.gitlab.com/juhani/go-semrel-gitlab:v0.21.1

#-- TEMPLATE [TYPE: except]
.tags_except:
  except:
    - tags

#-- TEMPLATE [TYPE: job]
.build_job:
  script:
    - mkdir images
    - TAG=$(if [[ -f ".version" ]]; then cat .version; else echo ""; fi;)
    - >
      if [[ -f "stage.json" ]]; then
        if [[ $(jq --raw-output 'has("review")' stage.json) = true ]]; then
          if [[ $(jq --raw-output '.review | has("build")' stage.json) = true ]]; then
            jq --raw-output '.review.build | keys[]' stage.json | while read -r build_index; do
              image=$(eval "echo \"$(jq --raw-output $(echo ".review.build[$build_index].image") stage.json)\"");
              name=$(eval "echo \"$(jq --raw-output $(echo ".review.build[$build_index].name") stage.json)\"");
              version=$(eval "echo \"$(jq --raw-output $(echo ".review.build[$build_index].version") stage.json)\"");
              docker pull $CI_REGISTRY_IMAGE/$name:$TAG || true;
              TAG=$TAG CI_REGISTRY_IMAGE=$CI_REGISTRY_IMAGE docker-compose build $name;
              if [[ $version = null ]]; then
                docker save $image:latest > images/$image.tar;
              else
                docker save $image:$version > images/$image.tar;
              fi;
            done;
          fi;
        fi;
      fi;

.deploy_job:
  script:
    - >
      if [[ -f "deploy.json" ]]; then
        if [[ -f ".next-version" ]]; then
          TAG=$(cat .next-version);
          if [[ $(jq --raw-output 'has("deploy")' deploy.json) = true ]]; then
            jq --raw-output '.deploy | keys[]' deploy.json | while read -r deploy_index; do
              branch_list=$(jq --raw-output $(echo ".deploy[$deploy_index].branch") deploy.json);
              service_list=$(jq --raw-output $(echo ".deploy[$deploy_index].service") deploy.json);
              if [[ "$branch_list" != null ]]; then
                echo "$branch_list" | jq --raw-output '.[]' | while read -r branch; do
                  if [[ "$CI_COMMIT_REF_NAME" = "$branch" ]]; then
                    echo "$service_list" | jq --raw-output '. | keys[]' | while read -r service_index; do
                      image=$(eval "echo \"$(jq --raw-output $(echo ".deploy[$deploy_index].service[$service_index].image") deploy.json)\"");
                      name=$(eval "echo \"$(jq --raw-output $(echo ".deploy[$deploy_index].service[$service_index].name") deploy.json)\"");
                      version=$(eval "echo \"$(jq --raw-output $(echo ".deploy[$deploy_index].service[$service_index].version") deploy.json)\"");
                      docker load -i images/$image.tar;
                      if [[ $version = null ]]; then
                        docker tag $image:latest $CI_REGISTRY_IMAGE/$name:$TAG;
                      else
                        docker tag $image:$version $CI_REGISTRY_IMAGE/$name:$TAG;
                      fi;
                      docker push $CI_REGISTRY_IMAGE/$name:$TAG;
                    done;
                  fi;
                done;
              fi;
            done;
          fi;
        fi;
      fi;

.merge_job:
  script:
    - >
      if [[ -f "stage.json" ]]; then
        release_branch=$(jq --raw-output ".release_branch" stage.json) || master;
        if [[ $(jq --raw-output 'has("merge")' stage.json) = true ]]; then
          if [[ $(jq --raw-output '.merge | has("merge")' stage.json) = true ]]; then
            jq --raw-output '.merge.merge | keys[]' stage.json | while read -r merge_index; do
              source=$(eval "echo \"$(jq --raw-output $(echo ".merge.merge[$merge_index].source") stage.json)\"");
              target_list=$(jq --raw-output $(echo ".merge.merge[$merge_index].target") stage.json);
              if [[ "$CI_COMMIT_REF_NAME" = "$source" ]]; then
                echo "$target_list" | jq --raw-output '. | keys[]' | while read -r target_index; do
                  target=$(eval "echo \"$(jq --raw-output $(echo ".merge.merge[$merge_index].target[$target_index].name") stage.json)\"");
                  protected=$(jq --raw-output $(echo ".merge.merge[$merge_index].target[$target_index].protected") stage.json);
                  if [[ $(curl -s -o /null -X GET -w "%{http_code}" -H "PRIVATE-TOKEN:$GL_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/repository/branches/$target") != 200 ]]; then
                    curl -s --request POST -H "PRIVATE-TOKEN:$GL_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/repository/branches?branch=$target&ref=$release_branch";
                  fi;
                  if [[ $protected != null ]]; then
                    echo "$protected" | jq --raw-output '. | "\(.merge)#\(.push)#\(.unprotect)"' | while IFS=# read -r merge push unprotect; do
                      PROTECT=https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/protected_branches?name=$target;
                      if [[ $merge != null ]]; then
                        PROTECT=$PROTECT\&merge_access_level=$merge;
                      fi;
                      if [[ $push != null ]]; then
                        PROTECT=$PROTECT\&push_access_level=$push;
                      fi;
                      if [[ $unprotect != null ]]; then
                        PROTECT=$PROTECT\&unprotect_access_level=$unprotect;
                      fi;
                      curl -s --request POST -H "PRIVATE-TOKEN:$GL_TOKEN" "$PROTECT";
                    done;
                  fi;
                  if [[ $(curl -s -X GET -H "PRIVATE-TOKEN:$GL_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/merge_requests?source_branch=$source&state=opened&target_branch=$target") = "[]" ]]; then
                    curl -s --request POST -H "PRIVATE-TOKEN:$GL_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/merge_requests?source_branch=$source&target_branch=$target&title=$source-$target";
                  fi;
                done;
              fi;
            done;
          fi;
        fi;
      fi;

.mirror_job:
  script:
    - TAG=$(if [[ -f ".next-version" ]]; then cat .next-version; else echo $CI_COMMIT_SHA; fi;)
    - git config user.email "$GITLAB_USER_EMAIL"
    - git config user.name "$GITLAB_USER_ID"
    - release_branch=$(if [[ -f "stage.json" ]]; then jq --raw-output ".release_branch" stage.json; fi;) || master
    - >
      if [[ -f "mirror.json" ]]; then
        if [[ $(jq --raw-output 'has("mirror")' mirror.json) = true ]]; then
          jq --raw-output '.mirror | keys[]' mirror.json | while read -r mirror_index; do
            id=$(eval "echo \"$(jq --raw-output $(echo ".mirror[$mirror_index].id") mirror.json)\"");
            branch_list=$(jq --raw-output $(echo ".mirror[$mirror_index].branch") mirror.json);
            if [[ $branch_list != null ]]; then
              echo "$branch_list" | jq --raw-output '. | keys[]' | while read -r branch_index; do
                branch=$(eval "echo \"$(jq --raw-output $(echo ".mirror[$mirror_index].branch[$branch_index].name") mirror.json)\"");
                exclude=$(eval "echo \"$(jq --raw-output $(echo ".mirror[$mirror_index].branch[$branch_index].exclude") mirror.json)\"");
                target_list=$(jq --raw-output $(echo ".mirror[$mirror_index].branch[$branch_index].target") mirror.json);
                if [[ "$CI_COMMIT_REF_NAME" = "$branch" ]]; then
                  echo "$target_list" | jq --raw-output '.[]' | while read -r target; do
                    if [[ $(curl -s -o /null -X GET -w "%{http_code}" -H "PRIVATE-TOKEN:$GL_TOKEN" "https://gitlab.com/api/v4/projects/$id/repository/branches/$target") != 200 ]]; then
                      curl -s --request POST -H "PRIVATE-TOKEN:$GL_TOKEN" "https://gitlab.com/api/v4/projects/$id/repository/branches?branch=$target&ref=$release_branch";
                    fi;
                    if [[ $(curl -s -o /null -X GET -w "%{http_code}" -H "PRIVATE-TOKEN:$GL_TOKEN" "https://gitlab.com/api/v4/projects/$id/repository/branches/update-$CI_PROJECT_ID-$branch-$target") != 200 ]]; then
                      curl -s --request POST -H "PRIVATE-TOKEN:$GL_TOKEN" "https://gitlab.com/api/v4/projects/$id/repository/branches?branch=update-$CI_PROJECT_ID-$branch-$target&ref=$target";
                    fi;
                    if [[ $(curl -s -X GET -H "PRIVATE-TOKEN:$GL_TOKEN" "https://gitlab.com/api/v4/projects/$id/merge_requests?source_branch=update-$CI_PROJECT_ID-$branch-$target&state=opened&target_branch=$target") = "[]" ]]; then
                      curl -s --request POST -H "PRIVATE-TOKEN:$GL_TOKEN" "https://gitlab.com/api/v4/projects/$id/merge_requests?source_branch=update-$CI_PROJECT_ID-$branch-$target&target_branch=$target&title=MIRROR-$CI_PROJECT_ID-$branch-$target";
                    fi;
                    git config --global user.email "<$GL_EMAIL>";
                    git config --global user.name "Mirror GitLab";
                    git clone -b update-$CI_PROJECT_ID-$branch-$target $(curl -s --request GET -H "PRIVATE-TOKEN:$GL_TOKEN" "https://gitlab.com/api/v4/projects/$id" | jq -r ".http_url_to_repo" | sed "s/https:\/\//https:\/\/gitlab-ci-token:$GL_TOKEN@/g") /mirror-$id;
                    rsync -aP --exclude=.git --delete-before ./ /mirror-$id
                    if [[ $exclude = null ]]; then
                      rm -rf /mirror-$id/.next-version /mirror-$id/.version /mirror-$id/CHANGELOG.md /mirror-$id/deploy.yml /mirror-$id/mirror.json;
                    else
                      rm -rf /mirror-$id/.next-version /mirror-$id/.version /mirror-$id/CHANGELOG.md /mirror-$id/deploy.yml /mirror-$id/mirror.json $exclude;
                    fi;
                    git -C /mirror-$id add --all;
                    git -C /mirror-$id commit -m "refactor($CI_PROJECT_NAME):$TAG BREAKING CHANGE:UPDATE";
                    git -C /mirror-$id push origin update-$CI_PROJECT_ID-$branch-$target;
                  done;
                fi;
              done;
            fi;
          done;
        fi;
      fi;

.release_job:
  script:
    - >
      if [[ -f ".next-version" ]]; then
        mv .next-version .version;
        if [[ -f "stage.json" ]]; then
          initial_development=$(jq --raw-output ".initial_development" stage.json) || true;
          release_branch=$(jq --raw-output ".release_branch" stage.json) || master;
          if [[ $(jq --raw-output 'has("release")' stage.json) = true ]]; then
            if [[ $(jq --raw-output '.release | has("release")' stage.json) = true ]]; then
              jq --raw-output '.release.release | keys[]' stage.json | while read -r release_index; do
                branch=$(eval "echo \"$(jq --raw-output $(echo ".release.release[$release_index].branch") stage.json)\"");
                commit=$(eval "echo \"$(jq --raw-output $(echo ".release.release[$release_index].commit") stage.json)\"");
                template=$(eval "echo \"$(jq --raw-output $(echo ".release.release[$release_index].template") stage.json)\"");
                if [[ "$CI_COMMIT_REF_NAME" = "$branch" ]]; then
                  if [[ $template = null ]]; then
                    GSG_INITIAL_DEVELOPMENT=$initial_development GSG_RELEASE_BRANCHES=$release_branch release changelog || true;
                    GSG_INITIAL_DEVELOPMENT=$initial_development GSG_RELEASE_BRANCHES=$release_branch release commit-and-tag $commit;
                  else
                    GSG_INITIAL_DEVELOPMENT=$initial_development GSG_PRE_TMPL="$template" GSG_RELEASE_BRANCHES=$release_branch release changelog || true;
                    GSG_INITIAL_DEVELOPMENT=$initial_development GSG_PRE_TMPL="$template" GSG_RELEASE_BRANCHES=$release_branch release commit-and-tag $commit;
                  fi;
                fi;
              done;
            fi;
          fi;
        fi;
      fi;

.version_job:
  script:
    - >
      if [[ -f "stage.json" ]]; then
        initial_development=$(jq --raw-output ".initial_development" stage.json) || true;
        release_branch=$(jq --raw-output ".release_branch" stage.json) || master;
        if [[ $(jq --raw-output 'has("version")' stage.json) = true ]]; then
          if [[ $(jq --raw-output '.version | has("version")' stage.json) = true ]]; then
            jq --raw-output '.version.version | keys[]' stage.json | while read -r version_index; do
              branch=$(eval "echo \"$(jq --raw-output $(echo ".version.version[$version_index].branch") stage.json)\"");
              template=$(eval "echo \"$(jq --raw-output $(echo ".version.version[$version_index].template") stage.json)\"");
              if [[ "$CI_COMMIT_REF_NAME" = "$branch" ]]; then
                if [[ $template = null ]]; then
                  GSG_INITIAL_DEVELOPMENT=$initial_development GSG_RELEASE_BRANCHES=$release_branch release next-version --allow-current > .next-version;
                else
                  GSG_INITIAL_DEVELOPMENT=$initial_development GSG_PRE_TMPL="$template" GSG_RELEASE_BRANCHES=$release_branch release next-version --allow-current > .next-version;
                fi;
              fi;
            done;
          fi;
        fi;
      fi;

#-- TEMPLATE [TYPE: only]
.merge-requests_only:
  only:
    - merge_requests

#-- TEMPLATE [TYPE: stage]
.deploy_stage:
  stage: deploy

.merge_stage:
  stage: merge

.mirror_stage:
  stage: mirror

.release_stage:
  stage: release

.review_stage:
  stage: review

.version_stage:
  stage: version

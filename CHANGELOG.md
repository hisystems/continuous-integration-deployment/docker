# CHANGELOG

<!--- next entry here -->

## 1.0.1
2020-01-28

### Fixes

- **ci:** Fixes git tags fetch. (55e665faae721d3371b813920002907ca6d4acc3)

## 1.0.0
2020-01-28

### Features

- **ci:** Initial Commit. (e87d4c501a0e5e1c04d171e6ee542cb07f13de0b)

### Fixes

- **project:** 1.0.1 BREAKING CHANGE:UPDATE (57afb6a4bfff1cb34283683b2af29a47dc3564c7)
- **doc:** Fixes README.md. (8f5179b2262e73036c0837023c42e266d9b3cb5e)
- **ci:** Fixes mirror.json. (459f393e131c8aea1c4e9ec6eadff2109e693688)
- **ci:** Update mirror.json. (42f7340136a5541d0573e095fb34bae87bad4c5d)
- **ci:** Update go-semrel-gitlab (v0.20.4 -> v0.21.1). (210fdfb3191733f1c01a9cbd702dbf34f7d66959)

## 1.0.0
2020-01-28

### Features

- **ci:** Initial Commit. (e87d4c501a0e5e1c04d171e6ee542cb07f13de0b)

### Fixes

- **project:** 1.0.1 BREAKING CHANGE:UPDATE (57afb6a4bfff1cb34283683b2af29a47dc3564c7)
- **doc:** Fixes README.md. (8f5179b2262e73036c0837023c42e266d9b3cb5e)
- **ci:** Fixes mirror.json. (459f393e131c8aea1c4e9ec6eadff2109e693688)
- **ci:** Update mirror.json. (42f7340136a5541d0573e095fb34bae87bad4c5d)

## 1.0.0
2020-01-10

### Features

- **ci:** Initial Commit. (e87d4c501a0e5e1c04d171e6ee542cb07f13de0b)

### Fixes

- **project:** 1.0.1 BREAKING CHANGE:UPDATE (57afb6a4bfff1cb34283683b2af29a47dc3564c7)
- **doc:** Fixes README.md. (8f5179b2262e73036c0837023c42e266d9b3cb5e)

## 1.0.0-beta.1
2020-01-10

### Features

- **ci:** Initial Commit. (e87d4c501a0e5e1c04d171e6ee542cb07f13de0b)

### Fixes

- **project:** 1.0.1 BREAKING CHANGE:UPDATE (57afb6a4bfff1cb34283683b2af29a47dc3564c7)

## 1.0.0-alpha.1
2020-01-10

### Features

- **ci:** Initial Commit. (e87d4c501a0e5e1c04d171e6ee542cb07f13de0b)

### Fixes

- **project:** 1.0.1 BREAKING CHANGE:UPDATE (57afb6a4bfff1cb34283683b2af29a47dc3564c7)